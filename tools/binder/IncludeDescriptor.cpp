#include "./IncludeDescriptor.h"

#include <fstream>
#include <iostream>

namespace binder {

static std::vector<std::string>
splitWords(std::istream& is) {
  std::vector<std::string> result;

  bool startNewWord = true;
  while (is.good()) {
    char c;
    if (is.read(&c, sizeof(c))) {
      switch (c) {
        case '\n':
        case '\r':
        case '\t':
        case ' ': {
          startNewWord = true;
          break;
        }

        case '(':
        case ')':
        case '[':
        case ']':
        case '&':
        case '*':
        case ',': {
          startNewWord = true;
          result.push_back(std::string(&c, &c + 1));
          break;
        }

        default: {
          if (startNewWord) {
            startNewWord = false;
            result.push_back("");
          }

          result.back() += c;
          break;
        }
      }
    }
  }

  return result;
}

IncludeDescriptor::IncludeDescriptor(std::filesystem::path const& path) {
  std::ifstream file(path);
  if (!file.is_open()) {
    throw std::runtime_error("Unable to open file \"" + path.string() + "\"");
  }

  auto words = splitWords(file);

  std::cout << "Parsing file " << path << std::endl;
  for (auto it = words.begin(), itEnd = words.end(); it != itEnd;) {
    //std::cout << "  '" << word << "'" << std::endl;

    /****/ if (*it == "class") {
      ++it;
      if (it != itEnd) { className = *it; };
    } else if (*it == "PERIPHERAL_DECLARE_TYPE") {
      ++it;
      if (it == itEnd || *it != "(") { throw std::runtime_error("Expecting '('"); };

      ++it;
      if (it != itEnd) { type = *it; };

      ++it;
      if (it == itEnd || *it != ")") { throw std::runtime_error("Expecting ')'"); };
    } else if (*it == "PERIPHERAL_DECLARE_GETTER") {
      ++it;
      if (it == itEnd || *it != "(") { throw std::runtime_error("Expecting '('"); };

      Getter getter;

      ++it;
      if (it != itEnd) { getter.name = *it; };

      ++it;
      if (it == itEnd || *it != ",") { throw std::runtime_error("Expecting ','"); };

      ++it;
      while (it != itEnd && *it != ")") {
        if (!getter.returnType.empty()) getter.returnType += " ";
        getter.returnType += *it;
        ++it;
      }

      if (it == itEnd || *it != ")") { throw std::runtime_error("Expecting ')'"); };

      getters.push_back(getter);
    } else if (*it == "PERIPHERAL_DECLARE_SETTER") {
      ++it;
      if (it == itEnd || *it != "(") { throw std::runtime_error("Expecting '('"); };

      Setter setter;

      ++it;
      if (it != itEnd) { setter.name = *it; };

      ++it;
      if (it == itEnd || *it != ",") { throw std::runtime_error("Expecting ','"); };

      ++it;
      while (it != itEnd && *it != ")") {
        if (!setter.valueType.empty()) setter.valueType += " ";
        setter.valueType += *it;
        ++it;
      }

      if (it == itEnd || *it != ")") { throw std::runtime_error("Expecting ')'"); };

      setters.push_back(setter);
    } else if (*it == "PERIPHERAL_DECLARE_CALLBACK") {
      ++it;
      if (it == itEnd || *it != "(") { throw std::runtime_error("Expecting '('"); };

      Callback callback;

      ++it;
      if (it != itEnd) { callback.name = *it; };

      ++it;
      if (it == itEnd || *it != ",") { throw std::runtime_error("Expecting ','"); };

      ++it;
      callback.args.emplace_back();
      while (it != itEnd && *it != ")") {
        if (*it == ",") {
          callback.args.emplace_back();
        } else {
          auto next = std::next(it);
          if (next == itEnd || *next == "," || *next == ")") {
            callback.args.back().name = *it;
          } else {
            if (!callback.args.back().type.empty()) callback.args.back().type += " ";
            callback.args.back().type += *it;
          }
        }
        ++it;
      }

      if (it == itEnd || *it != ")") { throw std::runtime_error("Expecting ')'"); };

      callbacks.push_back(callback);
    }

    if (it != itEnd) ++it;
  }

  // Add getters from AbstractPeripheral
  {
    getters.push_back({
        .name       = "getName",
        .returnType = "std::string const &",
    });

    getters.push_back({
        .name       = "toString",
        .returnType = "std::string",
    });

    getters.push_back({
        .name       = "getType",
        .returnType = "char const *",
    });
  }

  std::cout << "Class Name: " << className << std::endl;
  std::cout << "Type      : " << type << std::endl;
  std::cout << "Getters   : " << getters.size() << std::endl;
  std::cout << "Setters   : " << setters.size() << std::endl;
  std::cout << "Callbacks : " << callbacks.size() << std::endl;

  std::cout << "========================" << std::endl;
  std::cout << "Getters:" << std::endl;
  for (auto const& getter : getters) {
    std::cout << "  " << getter.name << " => " << getter.returnType << std::endl;
  }

  std::cout << "========================" << std::endl;
  std::cout << "Setters:" << std::endl;
  for (auto const& setter : setters) {
    std::cout << "  " << setter.name << " => " << setter.valueType << std::endl;
  }

  std::cout << "========================" << std::endl;
  std::cout << "Callbacks:" << std::endl;
  for (auto const& callback : callbacks) {
    std::cout << "  " << callback.name << "(";
    for (auto it = callback.args.begin(); it != callback.args.end();) {
      std::cout << "(" << it->type << ") " << it->name
                << ((++it == callback.args.end()) ? ")" : ", ");
    }
    std::cout << std::endl;
  }

  std::cout << "========================" << std::endl;
}

} // namespace binder