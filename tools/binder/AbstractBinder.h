#pragma once

#include <filesystem>
#include <memory>
#include <string>

#include "./IncludeDescriptor.h"

namespace binder {

class AbstractBinder {
 public:
  typedef std::shared_ptr<AbstractBinder> shared_ptr;

  static shared_ptr
  create(
      std::string const& language,
      std::filesystem::path const& root,
      std::filesystem::path const& lib,
      std::filesystem::path const& output);

  inline AbstractBinder(
      std::filesystem::path const& root,
      std::filesystem::path const& lib,
      std::filesystem::path const& output)
      : _root(root)
      , _lib(lib)
      , _output(output) {}

  virtual ~AbstractBinder(void);

  virtual int
  handlePeripheralInclude(IncludeDescriptor const& desc) = 0;

  virtual int
  handleCore(void) = 0;

  virtual int
  finalize(void) = 0;

  int
  run(void);

  inline std::filesystem::path const&
  root(void) const {
    return _root;
  }

  inline std::filesystem::path const&
  lib(void) const {
    return _lib;
  }

  inline std::filesystem::path
  includeDir(void) const {
    return _root / "include";
  }

  inline std::filesystem::path
  bsonDir(void) const {
    return _root / "src" / "libbson-mini" / "src";
  }

  inline std::filesystem::path const&
  output(void) const {
    return _output;
  }

  inline std::string
  author(void) const {
    return "Exceenis";
  }

  inline std::string
  extensionName(void) const {
    return "proxyos";
  }

  inline std::string
  description(void) const {
    return "Exceenis Gateway Proxy";
  }

  inline std::string
  projectUrl(void) const {
    return "";
  }

 private:
  std::filesystem::path _root;
  std::filesystem::path _lib;
  std::filesystem::path _output;
};

} // namespace binder