#include "./AbstractBinder.h"

#include <iostream>

#include "./lua/Binder.h"
#include "./python/Binder.h"

namespace binder {

AbstractBinder::shared_ptr
AbstractBinder::create(
    std::string const& language,
    std::filesystem::path const& root,
    std::filesystem::path const& lib,
    std::filesystem::path const& output) {
  /****/ if (language == "python") {
    return shared_ptr(new python::Binder(root, lib, output));
  } else if (language == "lua") {
    return shared_ptr(new lua::Binder(root, lib, output));
  }

  return shared_ptr();
}

AbstractBinder::~AbstractBinder(void) {}

int
AbstractBinder::run(void) {
  std::filesystem::path includePeripheral = _root / "include" / "proxyos" / "peripheral";

  if (!std::filesystem::is_directory(includePeripheral)) {
    std::cerr << "Unable to find peripheral include directory at " << includePeripheral
              << std::endl;
    return 1;
  }

  for (auto const& path : std::filesystem::directory_iterator(includePeripheral)) {
    int ret = 0;
    try {
      IncludeDescriptor desc(path);
      ret = handlePeripheralInclude(desc);
    } catch (std::exception const& exception) {
      std::cerr << "Handling peripheral include failed with error: " << exception.what()
                << std::endl;
      ret = -1;
    }

    if (ret != 0) {
      std::cerr << "Failed to handle peripheral include (" << ret << ")" << std::endl;
      return 2;
    }
  }

  {
    int ret = 0;
    try {
      ret = handleCore();
    } catch (std::exception const& exception) {
      std::cerr << "Handling core failed with error: " << exception.what() << std::endl;
      ret = -1;
    }

    if (ret != 0) {
      std::cerr << "Failed to handle core (" << ret << ")" << std::endl;
      return 3;
    }
  }

  {
    int ret = 0;
    try {
      ret = finalize();
    } catch (std::exception const& exception) {
      std::cerr << "Finalization failed with error: " << exception.what() << std::endl;
      ret = -1;
    }

    if (ret != 0) {
      std::cerr << "Failed to finalize (" << ret << ")" << std::endl;
      return 4;
    }
  }

  return 0;
}

} // namespace binder
