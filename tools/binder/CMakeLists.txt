project("binder")

file(GLOB_RECURSE SOURCE "*.cpp" "*.h" "*.c")
add_executable("${PROJECT_NAME}" ${SOURCE})
target_compile_features("${PROJECT_NAME}" PRIVATE cxx_std_17)
