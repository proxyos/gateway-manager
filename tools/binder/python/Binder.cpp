#include "./Binder.h"

#include <fstream>
#include <iostream>

//#define TRACE_OBJECT
//#define DECLARE_NEW

namespace binder {
namespace python {

Binder::~Binder(void) {}

int
Binder::handlePeripheralInclude(IncludeDescriptor const& desc) {
  _peripherals.push_back({
    name : desc.className,
    type : desc.type,
  });

  {
    int ret = createPeripheralDirectory();
    if (ret != 0) {
      std::cerr << "Unable to create peripheral directory" << std::endl;
      return ret;
    }
  }

  std::string srcFilename = desc.className + ".cpp";
  std::string incFilename = desc.className + ".h";

  std::filesystem::path srcPath = outputPeripheral() / srcFilename;
  std::filesystem::path incPath = outputPeripheral() / incFilename;

  std::ofstream srcFile(srcPath);
  if (!srcFile.is_open()) {
    std::cerr << "Unable to open file " << srcPath << std::endl;
    return 1;
  }

  std::ofstream incFile(incPath);
  if (!incFile.is_open()) {
    std::cerr << "Unable to open file " << incPath << std::endl;
    return 2;
  }

  incFile << "#pragma once\n"
             "\n"
             "#include <Python.h>\n"
             "\n"
             "#include <proxyos/AbstractPeripheral.h>\n"
             "\n"
             "namespace peripheral {\n"
          << "namespace " << desc.className << " {\n"
          << "\n"
             "PyObject* getClassDefinition(void);\n"
             "PyObject* fromNative(proxyos::AbstractPeripheral::shared_ptr const& ptr);\n"
             "\n"
             "}\n"
             "}\n"
             "\n";

  srcFile << "#include \"./" << incFilename << "\"\n"
          << "\n"
             "#include <new>\n"
             "\n"
             "#include <structmember.h>\n"
             "\n"
          << "#include <proxyos/peripheral/" << desc.className << ".h>\n"
          << "\n"
             "namespace peripheral {\n"
          << "namespace " << desc.className << " {\n"
          << "\n"
             "struct Data {\n"
             "  proxyos::AbstractPeripheral::shared_ptr ptr;\n"
             "};\n"
             "\n"
             "struct Object {\n"
             "  PyObject_HEAD\n"
             "  Data data;\n"
             "};\n"
             "\n"
             "static PyMemberDef object_members[] = {\n"
             "  {NULL},\n"
             "};\n"
             "\n";

  for (auto const& getter : desc.getters) {
    srcFile << "static PyObject*\n"
            << "getter_" << getter.name << "(Object *self, PyObject *args) {\n"
            << "  (void) args;\n"
               "  (void) self;\n"
               "  PyObject* result = NULL;\n";

    srcFile << "  if (self->data.ptr) result = ";

    if (getter.returnType == "std::string const &" || getter.returnType == "const std::string &" ||
        getter.returnType == "std::string") {
      srcFile << "Py_BuildValue(\"s\", self->data.ptr->as<proxyos::peripheral::" << desc.className
              << ">()." << getter.name << "().c_str());\n";
    } else if (getter.returnType == "char const *" || getter.returnType == "const char *") {
      srcFile << "Py_BuildValue(\"s\", self->data.ptr->as<proxyos::peripheral::" << desc.className
              << ">()." << getter.name << "());\n";
    } else if (getter.returnType == "int32_t" || getter.returnType == "int64_t") {
      srcFile << "Py_BuildValue(\"l\", (long) self->data.ptr->as<proxyos::peripheral::"
              << desc.className << ">()." << getter.name << "());\n";
    } else if (getter.returnType == "double") {
      srcFile << "Py_BuildValue(\"d\", self->data.ptr->as<proxyos::peripheral::" << desc.className
              << ">()." << getter.name << "());\n";
    } else if (getter.returnType == "bool") {
      srcFile << "self->data.ptr->as<proxyos::peripheral::" << desc.className << ">()."
              << getter.name << "() ? Py_True: Py_False;\n";
    } else {
      srcFile << "NULL;\n";
    }

    srcFile << "  if (!result) PyErr_SetString(PyExc_TypeError, \"Getter for type '"
            << getter.returnType << "' is not handled yet\");\n"
            << "  return result;\n"
               "}\n"
               "\n";
  }

  for (auto const& setter : desc.setters) {
    srcFile << "static PyObject*\n"
            << "setter_" << setter.name << "(Object *self, PyObject *args) {\n"
            << "  (void) args;\n"
               "  (void) self;\n"
               "\n"
               "  if (self->data.ptr) {";

    if (setter.valueType == "std::string const &" || setter.valueType == "const std::string &" ||
        setter.valueType == "std::string" || setter.valueType == "char const *" ||
        setter.valueType == "const char *") {
      srcFile << "    char* value = \"\";\n"
              << "    if (!PyArg_ParseTuple(args, \"s\", &value)) return NULL;\n"
              << "    self->data.ptr->as<proxyos::peripheral::" << desc.className << ">()."
              << setter.name << "(value);\n";
    } else if (setter.valueType == "int32_t" || setter.valueType == "int64_t") {
      srcFile << "    long value = 0;\n"
              << "    if (!PyArg_ParseTuple(args, \"l\", &value)) return NULL;\n"
              << "    self->data.ptr->as<proxyos::peripheral::" << desc.className << ">()."
              << setter.name << "(value);\n";
    } else if (setter.valueType == "double") {
      srcFile << "    double value = 0;\n"
              << "    if (!PyArg_ParseTuple(args, \"d\", &value)) return NULL;\n"
              << "    self->data.ptr->as<proxyos::peripheral::" << desc.className << ">()."
              << setter.name << "(value);\n";
    } else if (setter.valueType == "bool") {
      srcFile << "    int value = 0;\n"
              << "    if (!PyArg_ParseTuple(args, \"p\", &value)) return NULL;\n"
              << "    self->data.ptr->as<proxyos::peripheral::" << desc.className << ">()."
              << setter.name << "(value);\n";
    } else {
      srcFile << "PyErr_SetString(PyExc_TypeError, \"Setter for type '" << setter.valueType
              << "' is not handled yet\");\n"
              << "  return NULL;";
    }

    srcFile << "  }\n"
            << "\n"
            << "  Py_RETURN_NONE;\n"
               "}\n"
               "\n";
  }

  for (auto const& callback : desc.callbacks) {
    srcFile << "static PyObject*\n"
            << "callback_" << callback.name << "(Object *self, PyObject *args) {\n"
            << "  (void) args;\n"
               "  (void) self;\n";

#ifdef TRACE_OBJECT
    srcFile << "  printf(\"Setting new callback " << callback.name << " for " << desc.className
            << " on %p\\n\", self);\n";
#endif

    srcFile << "\n"
               "  PyObject* pyCb = NULL;\n"
               "  if (!PyArg_ParseTuple(args, \"O\", &pyCb) || !PyCallable_Check(pyCb)) {\n"
               "    PyErr_SetString(PyExc_TypeError, \"Argument should be callable\");\n"
               "    return NULL;\n"
               "  } else if (self->data.ptr) {\n"
               "    Py_INCREF(pyCb);\n"
            << "    self->data.ptr->as<proxyos::peripheral::" << desc.className << ">()."
            << callback.name << "([self, pyCb](auto* hal, ";

    auto const itEnd = callback.args.end();
    for (auto it = callback.args.begin(); it != itEnd; ++it) {
      auto next = std::next(it);
      srcFile << it->type << " arg_" << it->name;
      if (next != itEnd) srcFile << ", ";
    }

    srcFile << ") {\n"
               "      (void) hal;\n"
               "      PyGILState_STATE gstate = PyGILState_Ensure();\n";

#ifdef TRACE_OBJECT
    srcFile << "      printf(\"Callback " << callback.name << " for " << desc.className
            << " on %p\\n\", self);\n";
#endif

    for (auto it = callback.args.begin(); it != itEnd; ++it) {
      srcFile << "      (void) arg_" << it->name << ";\n";
    }

    srcFile << "      PyObject* pyArgs = Py_BuildValue(\"(";

    for (auto it = callback.args.begin(); it != itEnd; ++it) {
      if (it->type == "std::string const &" || it->type == "const std::string &" ||
          it->type == "std::string" || it->type == "char const *" || it->type == "const char *") {
        srcFile << "s";
      } else if (
          it->type == "int" || it->type == "long int" || it->type == "int long" ||
          it->type == "int32_t" || it->type == "int64_t") {
        srcFile << "l";
      } else if (it->type == "bool") {
        srcFile << "0";
      } else if (it->type == "float" || it->type == "double") {
        srcFile << "d";
      }
    }

    srcFile << ")\", ";

    for (auto it = callback.args.begin(); it != itEnd; ++it) {
      if (it->type == "std::string const &" || it->type == "const std::string &" ||
          it->type == "std::string") {
        srcFile << "arg_" << it->name << ".c_str()";
      } else if (it->type == "char const *" || it->type == "const char *") {
        srcFile << "arg_" << it->name;
      } else if (
          it->type == "int" || it->type == "long int" || it->type == "int long" ||
          it->type == "int32_t" || it->type == "int64_t") {
        srcFile << "(long) arg_" << it->name;
      } else if (it->type == "bool") {
        srcFile << "arg_" << it->name << " ? Py_True : Py_False";
      } else if (it->type == "float" || it->type == "double") {
        srcFile << "(double) arg_" << it->name;
      }

      auto next = std::next(it);
      if (next != itEnd) srcFile << ", ";
    }

    srcFile << ");\n"
            << "      PyObject* ret = PyObject_CallObject(pyCb, pyArgs);\n"
               "      Py_DECREF(ret);\n"
               "      PyGILState_Release(gstate);\n"
               "    });\n"
               "  }\n"
               "\n"
               "  Py_RETURN_NONE;\n"
               "}\n"
               "\n";
  }

  srcFile << "static PyMethodDef object_methods[] = {\n";

  for (auto const& getter : desc.getters) {
    srcFile << "  {\n"
            << "    \"" << getter.name << "\",\n"
            << "    (PyCFunction) getter_" << getter.name << ",\n"
            << "    METH_NOARGS,\n"
            << "    \"TODO(Franck)\",\n"
            << "  },\n";
  }

  for (auto const& setter : desc.setters) {
    srcFile << "  {\n"
            << "    \"" << setter.name << "\",\n"
            << "    (PyCFunction) setter_" << setter.name << ",\n"
            << "    METH_VARARGS,\n"
            << "    \"TODO(Franck)\",\n"
            << "  },\n";
  }

  for (auto const& callback : desc.callbacks) {
    srcFile << "  {\n"
            << "    \"" << callback.name << "\",\n"
            << "    (PyCFunction) callback_" << callback.name << ",\n"
            << "    METH_VARARGS,\n"
            << "    \"TODO(Franck)\",\n"
            << "  },\n";
  }

  srcFile << "  {NULL},\n"
             "};\n"
             "\n"
             "static PyGetSetDef object_getsetters[] = {\n"
             "  {NULL},\n"
             "};\n"
             "\n"
             "static PyObject *\n"
             "object_new(PyTypeObject *type, PyObject *args, PyObject *kwds) {\n"
             "  (void) args;\n"
             "  (void) kwds;\n"
             "\n"
             "  Object *self = (Object *) type->tp_alloc(type, 0);\n"
#ifdef TRACE_OBJECT
          << "  printf(\"Call new for " << desc.className << " on %p\\n\", self);\n"
#endif
          << "  if (self != NULL) {\n"
             "    new(&self->data) Data();\n"
             "  }\n"
             "\n"
             "  return (PyObject*) self;\n"
             "}\n"
             "\n"
             "static void\n"
             "object_dealloc(Object *self) {\n"
#ifdef TRACE_OBJECT
          << "  printf(\"Call dealloc for " << desc.className << " on %p\\n\", self);\n"
#endif
          << "  self->data.~Data();\n"
             "  Py_TYPE(self)->tp_free((PyObject *) self);\n"
             "}\n"
             "\n"
             "static PyTypeObject type = {\n"
             "  PyVarObject_HEAD_INIT(NULL, 0)\n"
          << "  .tp_name = \"" << desc.className << "\",\n"
          << "  .tp_basicsize = sizeof(Object),\n"
             "  .tp_itemsize = 0,\n"
             "  .tp_dealloc = (destructor) object_dealloc,\n"
             "  .tp_vectorcall_offset = 0,\n"
             "  .tp_getattr = NULL,\n"
             "  .tp_setattr = NULL,\n"
             "  .tp_as_async = NULL,\n"
             "  .tp_repr = NULL,\n"
             "  .tp_as_number = NULL,\n"
             "  .tp_as_sequence = NULL,\n"
             "  .tp_as_mapping = NULL,\n"
             "  .tp_hash = NULL,\n"
             "  .tp_call = NULL,\n"
             "  .tp_str = NULL,\n"
             "  .tp_getattro = NULL,\n"
             "  .tp_setattro = NULL,\n"
             "  .tp_as_buffer = NULL,\n"
             "  .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,\n"
             "  .tp_doc = \"TODO(Franck)\",\n"
             "  .tp_traverse = NULL,\n"
             "  .tp_clear = NULL,\n"
             "  .tp_richcompare = NULL,\n"
             "  .tp_weaklistoffset = 0,\n"
             "  .tp_iter = NULL,\n"
             "  .tp_iternext = NULL,\n"
             "  .tp_methods = object_methods,\n"
             "  .tp_members = object_members,\n"
             "  .tp_getset = object_getsetters,\n"
             "  .tp_base = NULL,\n"
             "  .tp_dict = NULL,\n"
             "  .tp_descr_get = NULL,\n"
             "  .tp_descr_set = NULL,\n"
             "  .tp_dictoffset = 0,\n"
             "  .tp_init = NULL,\n"
             "  .tp_alloc = NULL,\n"
             "  .tp_new = object_new,\n"
             "  .tp_free = NULL,\n"
             "  .tp_is_gc = NULL,\n"
             "  .tp_bases = NULL,\n"
             "  .tp_mro = NULL,\n"
             "  .tp_cache = NULL,\n"
             "  .tp_subclasses = NULL,\n"
             "  .tp_weaklist = NULL,\n"
             "  .tp_del = NULL,\n"
             "};\n"
             "\n"
             "PyObject* getClassDefinition(void) {\n"
             "  static bool init = false;\n"
             "  static bool ok = true;"
             "  if (!init) { init = true; if (PyType_Ready(&type) < 0) ok = false; }\n"
#ifdef TRACE_OBJECT
          << "  printf(\"Get class definition for " << desc.className << " at %p\\n\", &type);\n"
#endif
          << "  return ok ? (PyObject*) &type : NULL;\n"
             "}\n"
             "\n"
             "PyObject* fromNative(proxyos::AbstractPeripheral::shared_ptr const& ptr) {\n"
#ifdef TRACE_OBJECT
          << "  printf(\"Called fromNative for " << desc.className << "\\n\");\n"
#endif
          << "  PyObject* self = PyObject_CallObject((PyObject *) &type, NULL);\n"
             "  ((Object*)self)->data.ptr = ptr;\n"
             "  return (PyObject*) self;\n"
             "}\n"
             "\n"
             "}\n"
             "}\n"
             "\n";

  _sources.push_back(srcPath);
  return 0;
}

int
Binder::handleCore(void) {
  {
    int ret = writeWrapper();
    if (ret != 0) {
      std::cerr << "Unable to write " << wrapperPath() << std::endl;
      return 1;
    }
  }

  return 0;
}

int
Binder::finalize(void) {
  {
    int ret = writeSetupPy();
    if (ret != 0) {
      std::cerr << "Unable to write " << setupPyPath() << std::endl;
      return 1;
    }
  }

  return 0;
}

int
Binder::writeSetupPy(void) {
  std::ofstream out(setupPyPath());
  if (!out.is_open()) {
    std::cerr << "Unable to open " << setupPyPath() << std::endl;
    return 1;
  }

  out << "from distutils.core import setup, Extension\n"
         "\n"
      << "extension = Extension('" << extensionName() << "',\n"
      << "  include_dirs = [\n"
      << "    '" << includeDir().string() << "',\n"
      << "    '" << bsonDir().string() << "',\n"
      << "    '" << output().string() << "',\n"
      << "    '" << outputPeripheral().string() << "',\n"
      << "  ],\n"
         "  libraries= [\n"
         "    'pthread',\n"
         "  ],\n"
         "  extra_objects= [\n"
      << "    '" << lib().string() << "',\n"
      << "  ],\n"
         "  sources = [\n"
      << "    '" << wrapperPath().string() << "',\n";

  for (auto const& source : _sources) out << "    '" << source.string() << "',\n";

  out << "  ],\n"
         "  extra_compile_args=[\n"
         "    '-Wall',\n"
         "    '-Wextra',\n"
         "    '-Werror',\n"
         "    '-Wno-deprecated-declarations',\n"
         "    '-Wno-missing-field-initializers',\n"
         "    '-Wno-unused-function',\n"
         "  ])\n"
         "\n"
      << "setup(name = '" << extensionName() << "',\n"
      << "  version = '1.0',\n"
      << "  description= '" << description() << "',\n"
      << "  author='" << author() << "',\n"
      << "  url = '" << projectUrl() << "',\n"
      << "  ext_modules=[extension])\n";

  return 0;
}

int
Binder::writeWrapper(void) {
  std::ofstream out(wrapperPath());
  if (!out.is_open()) {
    std::cerr << "Unable to open " << wrapperPath() << std::endl;
    return 1;
  }

  out << "#include <Python.h>\n"
         "\n";

  // Declare methods
  {
    std::vector<PythonMethod> methods = writeMethods(out);

    out << "static PyMethodDef declared_methods[] = {\n";

    for (auto const& method : methods) {
      out << "    {\n"
          << "        \"" << method.name << "\",\n"
          << "        " << method.func << ",\n"
          << "        " << (method.args == 0 ? "METH_NOARGS" : "METH_VARARGS") << ",\n"
          << "        \"" << method.desc << "\",\n"
          << "    },\n";
    }

    out << "    {\n"
           "        NULL,\n"
           "        NULL,\n"
           "        0,\n"
           "        NULL,\n"
           "    },\n"
           "};\n"
           "\n";
  }

  out << "static struct PyModuleDef module_definition = {\n"
         "    PyModuleDef_HEAD_INIT,\n"
      << "    \"" << extensionName() << "\",\n"
      << "    \"" << description() << "\",\n"
      << "    -1,\n"
         "    declared_methods,\n"
         "    NULL,\n"
         "    NULL,\n"
         "    NULL,\n"
         "    NULL,\n"
         "};\n"
         "\n";

  for (auto const& peripheral : _peripherals) {
    std::string cls = peripheral.name;
    out << "#include \"./peripherals/" << cls << ".h\"\n";
  }

  out << "extern \"C\" PyMODINIT_FUNC\n"
      << "PyInit_" << extensionName() << "(void) {\n"
      << "  Py_Initialize();\n"
         "\n"
         "  PyObject* module = PyModule_Create(&module_definition);\n"
         "\n";

  for (auto const& peripheral : _peripherals) {
    std::string cls    = peripheral.name;
    std::string object = "peripheral::" + cls + "::getClassDefinition()";
    out << "  if (" << object << ") {\n"
        << "    Py_INCREF(" << object << ");\n"
        << "    if (PyModule_AddObject(module, \"" << cls << "\", " << object << ") < 0) {\n"
        << "        printf(\"Failed to add object of type '" << cls << "'\");\n"
        << "        Py_DECREF(" << object << ");\n"
        << "        Py_DECREF(module);\n"
           "        return NULL;\n"
           "    }\n"
           "  } else {\n"
        << "    printf(\"Object of type '" << cls << "' is not ready\");\n"
        << "  }\n";
  }

  out << "  return module;\n"
         "}\n";

  return 0;
}

std::vector<Binder::PythonMethod>
Binder::writeMethods(std::ostream& out) {
  std::vector<PythonMethod> result;

  // version
  {
    out << "#include <proxyos/config.h>\n"
           "\n"
           "static PyObject*\n"
           "version(PyObject* self, PyObject* args) {\n"
           "  (void) self;\n"
           "  (void) args;\n"
           "\n"
           "  return Py_BuildValue(\"s\", proxyos::version().c_str());\n"
           "}\n"
           "\n";

    result.push_back({
        .name = "version",
        .func = "version",
        .args = 0,
        .desc = "Return the " + extensionName() + " version",
    });
  }

  // GatewayManager
  {
    out << "#include <proxyos/GatewayManager.h>\n"
        << "\n"
        << "proxyos::GatewayManager gatewayManager;\n";

    // addGateway
    {
      out << "static PyObject*\n"
             "addGateway(PyObject* self, PyObject* args) {\n"
             "  (void) self;\n"
             "\n"
             "  char* device = NULL;\n"
             "\n"
             "  if (!PyArg_ParseTuple(args, \"s\", &device))\n"
             "    return NULL;\n"
             "\n"
             "  gatewayManager.addGateway(device);\n"
             "  Py_RETURN_NONE;\n"
             "}\n"
             "\n";

      result.push_back({
          .name = "addGateway",
          .func = "addGateway",
          .args = 1,
          .desc = "Add a new Gateway from give device",
      });
    }

#define BIND_NO_ARG(_name, _desc)                 \
  {                                               \
    out << "static PyObject*\n" #_name            \
           "(PyObject* self, PyObject* args) {\n" \
           "  (void) self;\n"                     \
           "  (void) args;\n"                     \
           "\n"                                   \
           "  gatewayManager." #_name             \
           "();\n"                                \
           "  Py_RETURN_NONE;\n"                  \
           "}\n"                                  \
           "\n";                                  \
                                                  \
    result.push_back({                            \
        .name = #_name,                           \
        .func = #_name,                           \
        .args = 0,                                \
        .desc = _desc,                            \
    });                                           \
  }

    BIND_NO_ARG(
        waitUntilAllPeriphalDeclaration, //
        "Wait for peripheral declaration from all gateways");

    BIND_NO_ARG(
        waitUntilAllDisconnected, //
        "Wait for all gateways to be disconnected");

    BIND_NO_ARG(
        startAll, //
        "Send start request to all peripheral");

    BIND_NO_ARG(
        stopAll, //
        "Send stop request to all peripheral");

    BIND_NO_ARG(
        disconnectAll, //
        "Send deconnection request to all peripheral");

#undef BIND_NO_ARG

    // getPeripherals
    {
      for (auto const& peripheral : _peripherals) {
        out << "#include \"./peripherals/" << peripheral.name << ".h\"\n";
      }

      out << "static PyObject*\n"
             "getPeripherals(PyObject* self, PyObject* args) {\n"
             "  (void) self;\n"
             "  (void) args;\n"
             "\n"
             "  std::vector<proxyos::AbstractPeripheral::shared_ptr> peripherals = \n"
             "    gatewayManager.getPeripherals();\n"
             "\n"
             "  PyObject* result = PyList_New(peripherals.size());\n"
             "  for (size_t i = 0; i < peripherals.size(); ++i) {\n"
             "    auto const& p = peripherals[i];\n"
             "    PyObject* value = NULL;\n"
             "    if (false) {\n";

      for (auto const& peripheral : _peripherals) {
        out << "    } else if (p && strcmp(p->getType(), \"" << peripheral.type << "\") == 0) {\n"
            << "      value = peripheral::" << peripheral.name << "::fromNative(p);\n";
      }

      out << "    }\n"
             "\n"
             "    if (value) PyList_SetItem(result, i, value);\n"
             "    else printf(\"Unable to find '%s' peripheral\\n\", p->getType());\n"
             "  }\n"
             "\n"
             "\n"
             "  return result;\n"
             "}\n"
             "\n";

      result.push_back({
          .name = "getPeripherals",
          .func = "getPeripherals",
          .args = 0,
          .desc = "List all connected peripherals",
      });
    }
  }

#ifdef DECLARE_NEW
  // new_<Class>
  for (auto const& peripheral : _peripherals) {
    out << "#include \"./peripherals/" << peripheral.name << ".h\"\n"
        << "\n"
           "static PyObject*\n"
        << "new_" << peripheral.name << "(PyObject* self, PyObject* args) {\n"
        << "  (void) self;\n"
           "  (void) args;\n"
           "\n"
        << "  return peripheral::" << peripheral.name
        << "::fromNative(proxyos::AbstractPeripheral::shared_ptr());\n"
        << "}\n"
           "\n";

    result.push_back({
        .name = "new_" + peripheral.name,
        .func = "new_" + peripheral.name,
        .args = 0,
        .desc = "Return a new " + peripheral.name,
    });
  }
#endif

  return result;
}

int
Binder::createPeripheralDirectory(void) {
  if (std::filesystem::is_directory(outputPeripheral())) return 0;

  if (std::filesystem::exists(outputPeripheral())) {
    std::error_code error;
    std::filesystem::remove_all(outputPeripheral(), error);
    if (error) {
      std::cerr << "Failed to remove directory " << outputPeripheral() << ": " << error.message()
                << std::endl;
      return 1;
    }
  }

  std::error_code error;
  std::filesystem::create_directory(outputPeripheral(), error);
  if (error) {
    std::cerr << "Failed to create directory " << outputPeripheral() << ": " << error.message()
              << std::endl;
    return 2;
  }

  return 0;
}

} // namespace python
} // namespace binder