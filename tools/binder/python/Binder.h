#include "../AbstractBinder.h"

#include <filesystem>

namespace binder {
namespace python {

class Binder : public AbstractBinder {
 public:
  inline Binder(
      std::filesystem::path const& root,
      std::filesystem::path const& lib,
      std::filesystem::path const& output)
      : AbstractBinder(root, lib, output) {}

  ~Binder(void);

  int
  handlePeripheralInclude(IncludeDescriptor const& desc) override;

  int
  handleCore(void) override;

  int
  finalize(void) override;

 private:
  inline std::filesystem::path
  setupPyPath(void) const {
    return output() / "setup.py";
  }

  inline std::filesystem::path
  wrapperPath(void) const {
    return output() / "wrapper.cpp";
  }

  inline std::filesystem::path
  outputPeripheral(void) const {
    return output() / "peripherals";
  }

  int
  createPeripheralDirectory(void);

  int
  writeSetupPy(void);

  int
  writeWrapper(void);

  struct PythonMethod {
    std::string name;
    std::string func;
    int args;
    std::string desc;
  };

  std::vector<PythonMethod>
  writeMethods(std::ostream& out);

  struct PythonClass {
    std::string name;
    std::string type;
  };
  std::vector<PythonClass> _peripherals;

  std::vector<std::filesystem::path> _sources;
};

} // namespace python
} // namespace binder