import proxyos
import time

class Handlers(object):
  def led(self, p):
    print('max pwm:', p.getMaxPWM())
    print('color  :', p.getColor())

    print('Setting PWM to 500')
    p.setPWM(500)

  def ntc(self, p):
    print('unit   :', p.getUnit())

    print('Setting temperature callback')
    p.atTemperature(lambda x: print("temperature=%d" % x))

  def servo(self, p):
    print('max degree:', p.getMaxDegree())

    print('Setting degree to 0 then max')
    p.setDegree(0)
    time.sleep(1)
    p.setDegree(p.getMaxDegree())

handlers = Handlers()

proxyos.addGateway('/dev/ttyUSB0')
proxyos.waitUntilAllPeriphalDeclaration()
proxyos.startAll()

print("proxyos: ", proxyos.__dict__)

peripherals = proxyos.getPeripherals()
for p in peripherals:
  print("====================")

  print('class  :', p.__class__)
  print('dict   :', p.__class__.__dict__)
  print('name   :', p.getName())
  print('type   :', p.getType())
  print('desc   :', p.toString())

  handler = getattr(handlers, p.getType(), None)
  if handler == None:
    print("No handler for type '%s'" % p.getType())
  else:
    handler(p)

print("====================")

time.sleep(100)

proxyos.stopAll()
proxyos.disconnectAll()
proxyos.waitUntilAllDisconnected()
