#pragma once

#include <filesystem>
#include <vector>

namespace binder {

struct IncludeDescriptor {
  IncludeDescriptor(std::filesystem::path const& path);

  std::string className;
  std::string type;

  struct Getter {
    std::string name;
    std::string returnType;
  };
  std::vector<Getter> getters;

  struct Setter {
    std::string name;
    std::string valueType;
  };
  std::vector<Setter> setters;

  struct Argument {
    std::string type;
    std::string name;
  };
  struct Callback {
    std::string name;
    std::vector<Argument> args;
  };
  std::vector<Callback> callbacks;
};

} // namespace binder