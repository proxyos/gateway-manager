#include "./Binder.h"

namespace binder {
namespace lua {

Binder::~Binder(void) {}

int
Binder::handlePeripheralInclude(IncludeDescriptor const& desc) {
  return 0;
}

int
Binder::handleCore(void) {
  return 0;
}

int
Binder::finalize(void) {
  throw std::runtime_error("Not yet implemented");
  return 0;
}

} // namespace lua
} // namespace binder