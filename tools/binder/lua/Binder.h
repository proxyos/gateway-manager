#include "../AbstractBinder.h"

namespace binder {
namespace lua {

class Binder : public AbstractBinder {
 public:
  inline Binder(
      std::filesystem::path const& root,
      std::filesystem::path const& lib,
      std::filesystem::path const& output)
      : AbstractBinder(root, lib, output) {}

  ~Binder(void);

  int
  handlePeripheralInclude(IncludeDescriptor const& desc) override;

  int
  handleCore(void) override;

  int
  finalize(void) override;
};

} // namespace lua
} // namespace binder