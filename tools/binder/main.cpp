#include <filesystem>
#include <fstream>
#include <iostream>
#include <string>

#include "./AbstractBinder.h"

int
main(int argc, char** argv) {
  if (argc < 5) {
    std::cerr << "Usage: " << argv[0]
              << " <language> <project_root> <project_static_library> <output_directory>"
              << std::endl;
    return -1;
  }

  std::string language              = argv[1];
  std::filesystem::path projectRoot = std::filesystem::canonical(argv[2]);
  std::filesystem::path projectLib  = std::filesystem::canonical(argv[3]);
  std::filesystem::path outputDir   = argv[4];

  if (std::filesystem::exists(outputDir)) std::filesystem::remove_all(outputDir);
  std::filesystem::create_directory(outputDir);
  outputDir = std::filesystem::canonical(outputDir);
  std::cout << "Output directory " << outputDir << " has been reset" << std::endl;

  auto binderPtr = binder::AbstractBinder::create(language, projectRoot, projectLib, outputDir);
  if (!binderPtr) {
    std::cerr << "Language '" << language << "' is not yet supported" << std::endl;
    return -2;
  }

  return binderPtr->run();
}
