#include <fstream>
#include <iostream>

#include <proxyos/Gateway.h>
#include <proxyos/GatewayManager.h>

int
main(int argc, char** argv) {
  if (argc < 3) {
    std::cerr << "Usage: " << argv[0] << " <device_path> <device_tree>" << std::endl;
    return 1;
  }

  proxyos::GatewayManager gm;
  proxyos::Gateway::shared_ptr gateway = gm.addGateway(argv[1]);
  if (!gateway) {
    std::cerr << "Unable to connect to gateway via '" << argv[1] << "'" << std::endl;
    return 2;
  }

  std::vector<proxyos::byte_t> deviceTreeData;
  {
    std::ifstream dtFile(argv[2]);
    if (!dtFile.is_open()) {
      std::cerr << "Unable to open device tree file '" << argv[2] << "'" << std::endl;
      return 3;
    }

    dtFile.seekg(0, std::ios::end);
    size_t size = dtFile.tellg();
    dtFile.seekg(0);
    deviceTreeData.resize(size);
    dtFile.read(deviceTreeData.data(), size);
  }

  bson::Object deviceTree = bson::decode(deviceTreeData.data());
  gateway->sendDeviceTree(deviceTree);
  gateway->restart();

  return 0;
}