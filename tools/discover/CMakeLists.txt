project("discover")

file(GLOB SOURCE "*.cpp" "*.h" "*.c")
add_executable("${PROJECT_NAME}" ${SOURCE})
target_link_libraries("${PROJECT_NAME}" "proxyos")
