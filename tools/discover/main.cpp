#include <unistd.h>

#include <csignal>
#include <cstdio>
#include <cstring>
#include <iostream>
#include <thread>

#include <proxyos/AbstractChannel.h>
#include <proxyos/GatewayManager.h>

#include <proxyos/peripheral/LED.h>
#include <proxyos/peripheral/NTC.h>
#include <proxyos/peripheral/Servomotor.h>

static std::shared_ptr<proxyos::GatewayManager> gm;
static bool stopRequested = false;

static void
signalHandler(int sig) {
  std::cout << std::endl;

  switch (sig) {
    case SIGINT:
    case SIGTERM: {
      static int count = 0;
      stopRequested    = true;

      if (count == 1) {
        std::cout << "Second interruption request => force exit" << std::endl;
        exit(-1);
      }

      ++count;
      if (gm) {
        gm->stopAll();
        gm->disconnectAll();
      }

      break;
    }
  }
}

int
main(int argc, char const* const* argv) {
  if (argc < 2) {
    std::cerr << "Usage: " << argv[0] << " <device_path>" << std::endl;
    return 1;
  }

  ::signal(SIGTERM, signalHandler);
  ::signal(SIGINT, signalHandler);

  gm.reset(new proxyos::GatewayManager);
  if (!gm->addGateway(argv[1])) {
    std::cerr << "Unable to connect to gateway via '" << argv[1] << "'" << std::endl;
    return 2;
  }

  gm->waitUntilAllPeriphalDeclaration();
  gm->startAll();

  auto peripherals = gm->getPeripherals();
  std::cout << "Got " << peripherals.size() << " peripherals" << std::endl;

  for (auto const& peripheral : peripherals) {
    std::cout << "peripheral: '" << peripheral->getName() << "' is a '" << peripheral->getType()
              << "'" << std::endl;

    /* NTC
     * Display temperature when it changes
     */
    if (peripheral->is<proxyos::peripheral::NTC>()) {
      auto& ntc = peripheral->as<proxyos::peripheral::NTC>();
      std::cout << "NTC unit is '" << ntc.getUnit() << "'" << std::endl;
      ntc.atTemperature([](proxyos::peripheral::NTC* ntc, int32_t temperature) {
        std::cout << "temperature: " << temperature << std::endl;
      });
    }

    /* LED
     * On/off every second
     */
    if (peripheral->is<proxyos::peripheral::LED>()) {
      std::thread([peripheral]() {
        auto& led = peripheral->as<proxyos::peripheral::LED>();
        std::cout << "LED color is '" << led.getColor() << "'" << std::endl;
        bool state = true;
        while (!stopRequested) {
          if (state) {
            led.setPWM(0);
          } else {
            led.setPWM(led.getMaxPWM());
          }

          state = !state;
          sleep(1);
        }
      }).detach();
    }

    /* Servomotor
     * Loop : min. -> 1/4 -> 1/2 -> 3/4 -> max. -> 3/4 -> 1/2 -> 1/4
     */
    if (peripheral->is<proxyos::peripheral::Servomotor>()) {
      std::thread([peripheral]() {
        auto& servomotor = peripheral->as<proxyos::peripheral::Servomotor>();
        int step         = 0;
        while (!stopRequested) {
          if (step % 8 == 0) {
            servomotor.setDegree(0);
          } else if (step % 8 == 1) {
            servomotor.setDegree((int32_t)(servomotor.getMaxDegree() * 1 / 4));

          } else if (step % 8 == 2) {
            servomotor.setDegree((int32_t)(servomotor.getMaxDegree() * 2 / 4));

          } else if (step % 8 == 3) {
            servomotor.setDegree((int32_t)(servomotor.getMaxDegree() * 3 / 4));

          } else if (step % 8 == 4) {
            servomotor.setDegree((int32_t)(servomotor.getMaxDegree() * 4 / 4));

          } else if (step % 8 == 5) {
            servomotor.setDegree((int32_t)(servomotor.getMaxDegree() * 3 / 4));

          } else if (step % 8 == 6) {
            servomotor.setDegree((int32_t)(servomotor.getMaxDegree() * 2 / 4));

          } else if (step % 8 == 7) {
            servomotor.setDegree((int32_t)(servomotor.getMaxDegree() * 1 / 4));
          }

          step++;
          sleep(1);
        }
      }).detach();
    }
  }

  gm->waitUntilAllDisconnected();

  std::cout << "Goodbye!" << std::endl;
  return 0;
}
