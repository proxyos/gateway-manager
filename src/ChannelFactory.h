#pragma once

#include <memory>
#include <string>

#include <proxyos/AbstractChannel.h>

namespace proxyos {

class ChannelFactory {
 public:
  static AbstractChannel::shared_ptr
  create(std::string const& device);
};

} // namespace proxyos
