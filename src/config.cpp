#include <proxyos/config.h>

namespace proxyos {

std::string
version(void) {
  return VERSION_STRING;
}

} // namespace proxyos
