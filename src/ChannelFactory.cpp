#include "./ChannelFactory.h"

#include <cstring>
#include <iostream>

#include "./channel/UART.h"
#include "./channel/USB.h"

namespace proxyos {

#define DEVICE_UART_STR "/dev/tty"
#define UART_BAUDRATE 115200
#define DEVICE_USB_STR "/dev/bus/usb"

AbstractChannel::shared_ptr
ChannelFactory::create(std::string const& device) {
  /****/ if (strncmp(device.c_str(), DEVICE_UART_STR, strlen(DEVICE_UART_STR)) == 0) {
    return std::make_shared<channel::UART>(device, UART_BAUDRATE);
  } else if (strncmp(device.c_str(), DEVICE_USB_STR, strlen(DEVICE_USB_STR)) == 0) {
    return std::make_shared<channel::USB>(device);
  }

  return AbstractChannel::shared_ptr();
}

} // namespace proxyos
