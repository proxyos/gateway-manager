#include <proxyos/AbstractPeripheral.h>

#include <bson.hpp>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include <proxyos/logger.h>

#include <proxyos/Gateway.h>

static char const* const TAG = "AbstractPeripheral";

namespace proxyos {

AbstractPeripheral::~AbstractPeripheral(void) {}

void
AbstractPeripheral::onDataReceived(bson::Object const& obj) {
  bson::Variant const& typeVar = obj["tp"];
  if (typeVar.getType() != BSON_STRING) {
    LOGE(TAG, "Data should contain a 'tp' string field => skip");
    return;
  }

  char const* const type = typeVar.asString();
  auto const it          = _callbacks.find(type);
  if (it == _callbacks.end()) {
    LOGD(TAG, "No callback for type '%s'", type);
    return;
  }

  for (auto const& cb : it->second) cb(this, obj);
}

void
AbstractPeripheral::at(std::string const& messageType, data_callback_t const& cb) {
  _callbacks[messageType].push_back(cb);
}

void
AbstractPeripheral::writeHeader(char const* type, bson::Object& obj) const {
  obj["tp"] = type;
  obj["pr"] = _id;
}

void
AbstractPeripheral::send(bson::Object const& obj) {
  if (_parent) _parent->send(obj);
}

} // namespace proxyos