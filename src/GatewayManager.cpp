#include <proxyos/GatewayManager.h>

#include <cstring>
#include <functional>
#include <thread>

#include <bson.hpp>

#include <proxyos/AbstractChannel.h>
#include <proxyos/AbstractPeripheral.h>
#include <proxyos/Gateway.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include <proxyos/logger.h>

#include "./ChannelFactory.h"

static char const* const TAG = "GatewayManager";

namespace proxyos {

Gateway::shared_ptr
GatewayManager::addGateway(std::string const& device) {
  AbstractChannel::shared_ptr ptr = ChannelFactory::create(device);
  if (!ptr) return Gateway::shared_ptr();

  Gateway::shared_ptr gw_ptr = std::make_shared<Gateway>(ptr);
  _gateways[device]          = gw_ptr;

  return gw_ptr;
}

std::vector<AbstractPeripheral::shared_ptr>
GatewayManager::getPeripherals(void) const {
  std::vector<AbstractPeripheral::shared_ptr> result;

  for (auto const& value : _gateways) {
    for (auto const& peripheral : value.second->peripherals()) {
      result.push_back(peripheral.second);
    }
  }

  return result;
}

void
GatewayManager::disconnectAll(void) {
  for (auto const& value : _gateways) value.second->disconnect();
}

void
GatewayManager::waitUntilAllDisconnected(void) {
  for (auto const& value : _gateways) value.second->waitUntilDisconnected();
}

void
GatewayManager::waitUntilAllPeriphalDeclaration(void) {
  for (auto const& value : _gateways) value.second->waitUntilPeripheralDeclaration();
}

void
GatewayManager::startAll(void) {
  for (auto const& value : _gateways) value.second->start();
}

void
GatewayManager::stopAll(void) {
  for (auto const& value : _gateways) value.second->stop();
}

} // namespace proxyos