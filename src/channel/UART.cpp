#include "./UART.h"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#include <termios.h>
#include <unistd.h>

#include <cerrno>
#include <cstring>
#include <iostream>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include <proxyos/logger.h>

static char const* const TAG = "UART";

namespace proxyos {
namespace channel {

__attribute__((always_inline)) static inline int
baudrate_from_speed(int speed) {
  switch (speed) {
    case 9600: return B9600;
    case 19200: return B19200;
    case 38400: return B38400;
    case 57600: return B57600;
    case 115200: return B115200;
    case 230400: return B230400;
    case 460800: return B460800;
    case 500000: return B500000;
    case 576000: return B576000;
    case 921600: return B921600;
    case 1000000: return B1000000;
    case 1152000: return B1152000;
    case 1500000: return B1500000;
    case 2000000: return B2000000;
    case 2500000: return B2500000;
    case 3000000: return B3000000;
    case 3500000: return B3500000;
    case 4000000: return B4000000;
    default: return -1;
  }
}

__attribute__((always_inline)) static inline int
baudrate_to_speed(int baud) {
  switch (baud) {
    case B9600: return 9600;
    case B19200: return 19200;
    case B38400: return 38400;
    case B57600: return 57600;
    case B115200: return 115200;
    case B230400: return 230400;
    case B460800: return 460800;
    case B500000: return 500000;
    case B576000: return 576000;
    case B921600: return 921600;
    case B1000000: return 1000000;
    case B1152000: return 1152000;
    case B1500000: return 1500000;
    case B2000000: return 2000000;
    case B2500000: return 2500000;
    case B3000000: return 3000000;
    case B3500000: return 3500000;
    case B4000000: return 4000000;
    default: return -1;
  }
}

UART::UART(std::string const& device, int speed)
    : AbstractChannel(device + "@" + std::to_string(speed))
    , _valid(true) {
  LOGI(TAG, "Using serial port '%s' at speed %d", device.c_str(), speed);
  _fd = ::open(device.c_str(), O_RDWR);

  if (_fd >= 0) {
    LOGI(TAG, "Serial port open at '%s'", device.c_str());
  } else {
    LOGE(TAG, "Unable to open serial port at '%s'", device.c_str());
  }

  int ret = fdSetup(baudrate_from_speed(speed));
  if (ret != 0) {
    LOGE(TAG, "Failed to setup uart fd");

    _valid = false;
  }
}

UART::~UART(void) { ::close(_fd); }

bool
UART::isValid(void) const {
  return _valid;
}

ssize_t
UART::read(byte_t* buffer, std::size_t buffer_len, int timeout_ms) {
  LOGD(TAG, "Asking to read %lu bytes", (long unsigned) buffer_len);
  byte_t* start_buffer = buffer;
  for (int i = 0; i < timeout_ms / 100 && buffer_len > 0; i++) {
    int ret = ::read(_fd, buffer, buffer_len);
    LOGD(TAG, "Read %d bytes", ret);
    if (ret == 0) {
      // Timeout
    } else if (ret > 0) {
      i = 0;
      buffer += ret;
      buffer_len -= ret;
    } else {
      return ret;
    }
  }
  return (buffer - start_buffer);
}

ssize_t
UART::write(byte_t const* buffer, std::size_t buffer_len) {
  return ::write(_fd, buffer, buffer_len);
}

void
UART::purge(void) {
  uint8_t buffer[32];
  while (true) {
    int num_bytes = ::read(_fd, buffer, sizeof(buffer));

    /****/ if (num_bytes < 0) {
      LOGE(TAG, "Error reading: %s", strerror(errno));
      return;
    } else if (num_bytes == 0) {
      return;
    }
  }
}

int
UART::fdSetup(int baudrate) {
  struct termios tty;

  if (tcgetattr(_fd, &tty) != 0) {
    printf("Error %i from tcgetattr: %s \r\n", errno, strerror(errno));
    return 1;
  }

  tty.c_cflag &= ~PARENB;
  tty.c_cflag &= ~CSTOPB;
  tty.c_cflag &= ~CSIZE;
  tty.c_cflag |= CS8;
  tty.c_cflag &= ~CRTSCTS;
  tty.c_cflag |= CREAD | CLOCAL;

  tty.c_lflag &= ~ICANON;
  tty.c_lflag &= ~ECHO;
  tty.c_lflag &= ~ECHOE;
  tty.c_lflag &= ~ECHONL;
  tty.c_lflag &= ~ISIG;
  tty.c_iflag &= ~(IXON | IXOFF | IXANY);
  tty.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL);

  tty.c_oflag &= ~OPOST;
  tty.c_oflag &= ~ONLCR;

  tty.c_cc[VTIME] = 1;
  tty.c_cc[VMIN]  = 0;

  cfsetispeed(&tty, baudrate);
  cfsetospeed(&tty, baudrate);

  if (tcsetattr(_fd, TCSANOW, &tty) != 0) {
    printf("Error %i from tcsetattr: %s \r\n", errno, strerror(errno));
    return 2;
  }

  return 0;
}

} // namespace channel
} // namespace proxyos
