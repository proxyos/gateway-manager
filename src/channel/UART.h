#include <proxyos/AbstractChannel.h>

#include <cstddef>
#include <string>

namespace proxyos {
namespace channel {

class UART : public AbstractChannel {
 public:
  UART(std::string const& device, int speed);

  ~UART(void);

  bool
  isValid(void) const override;

  ssize_t
  read(byte_t* buffer, std::size_t buffer_len, int timeout_ms) override;

  ssize_t
  write(byte_t const* buffer, std::size_t buffer_len) override;

  void
  purge(void) override;

 private:
  int
  fdSetup(int baudrate);

  int _fd;
  bool _valid;
};

} // namespace channel
} // namespace proxyos
