#include "./USB.h"

namespace proxyos {
namespace channel {

USB::USB(std::string const& device)
    : AbstractChannel(device) {}

USB::~USB(void) {}

bool
USB::isValid(void) const {
  return false;
}

ssize_t
USB::read(byte_t* buffer, std::size_t buffer_len, int timeout_ms) {
  (void) buffer;
  (void) buffer_len;
  (void) timeout_ms;
  return -1;
}

ssize_t
USB::write(byte_t const* buffer, std::size_t buffer_len) {
  (void) buffer;
  (void) buffer_len;
  return -1;
}

void
USB::purge(void) {}

} // namespace channel
} // namespace proxyos