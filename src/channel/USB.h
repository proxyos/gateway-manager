#pragma once

#include <string>

#include <proxyos/AbstractChannel.h>

namespace proxyos {
namespace channel {

class USB : public AbstractChannel {
 public:
  USB(std::string const& device);

  ~USB(void);

  bool
  isValid(void) const override;

  ssize_t
  read(byte_t* buffer, std::size_t buffer_len, int timeout_ms) override;

  ssize_t
  write(byte_t const* buffer, std::size_t buffer_len) override;

  void
  purge(void) override;
};

} // namespace channel
} // namespace proxyos