#include <proxyos/peripheral/NTC.h>

#include <bson.hpp>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include <proxyos/logger.h>

#include <proxyos/Gateway.h>

static char const* const TAG = "NTC";

namespace proxyos {
namespace peripheral {

NTC::NTC(std::string const& unit)
    : _unit(unit) {}

std::string
NTC::toString(void) const {
  return "NTC(channel='" + (getParent() ? getParent()->getChannelName() : "null") +
         "',id=" + std::to_string(getId()) + ",name='" + getName() + "',unit='" + getUnit() + "')";
}

PERIPHERAL_IMPLEMENT_TYPE(NTC, obj) {
  bson::Variant const& unitVar = obj["unit"];
  if (unitVar.getType() != BSON_STRING) {
    LOGE(TAG, "Missing 'unit' string field in NTC declaration => skip");
    return shared_ptr();
  }

  return shared_ptr(new NTC(unitVar.asString()));
}

PERIPHERAL_IMPLEMENT_GETTER(NTC, getUnit, std::string const&) { return _unit; }

PERIPHERAL_IMPLEMENT_CALLBACK(NTC, atTemperature, cb, int32_t temperature) {
  at("temperature", [this, cb](AbstractPeripheral* periph, bson::Object const& obj) {
    auto const& var = obj["value"];
    if (var.getType() == BSON_INT32) {
      int32_t value = obj["value"].asInt32();
      LOGD(TAG, "New temperature: %ld", (long) value);
      cb(this, value);
    }
  });
}

} // namespace peripheral
} // namespace proxyos
