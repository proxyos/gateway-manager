#include <proxyos/peripheral/Servomotor.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include <proxyos/logger.h>

#include <proxyos/Gateway.h>

static char const* const TAG = "Servomotor";

namespace proxyos {
namespace peripheral {

Servomotor::Servomotor(double maxDegree)
    : _maxDegree(maxDegree) {}

std::string
Servomotor::toString(void) const {
  return "Servomotor(channel='" + (getParent() ? getParent()->getChannelName() : "null") +
         "',id=" + std::to_string(getId()) + ",name='" + getName() +
         "',maxDegree=" + std::to_string(getMaxDegree()) + ")";
}

PERIPHERAL_IMPLEMENT_TYPE(Servomotor, obj) {
  bson::Variant const& maxDegreeVar = obj["max_degree"];
  if (maxDegreeVar.getType() != BSON_DOUBLE) {
    LOGE(TAG, "Missing 'max_degree' double field in Servomotor declaration => skip");
    return shared_ptr();
  }

  return shared_ptr(new Servomotor(maxDegreeVar.asDouble()));
}

PERIPHERAL_IMPLEMENT_GETTER(Servomotor, getMaxDegree, double) { return _maxDegree; }

PERIPHERAL_IMPLEMENT_SETTER(Servomotor, setDegree, double degree) {
  LOGD(TAG, "Setting degree to %f", (float) degree);
  bson::Object obj;
  writeHeader("set_degree", obj);
  obj["value"] = degree;
  send(obj);
}

} // namespace peripheral
} // namespace proxyos
