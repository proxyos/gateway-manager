#include <proxyos/peripheral/LED.h>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include <proxyos/logger.h>

#include <proxyos/Gateway.h>

static char const* const TAG = "LED";

namespace proxyos {
namespace peripheral {

LED::LED(std::string const& color, int32_t maxPWM)
    : _color(color)
    , _maxPWM(maxPWM) {}

std::string
LED::toString(void) const {
  return "LED(channel='" + (getParent() ? getParent()->getChannelName() : "null") +
         "',id=" + std::to_string(getId()) + ",name='" + getName() + "',color='" + getColor() +
         "',maxPWM=" + std::to_string(getMaxPWM()) + ")";
}

PERIPHERAL_IMPLEMENT_TYPE(LED, obj) {
  bson::Variant const& colorVar = obj["color"];
  if (colorVar.getType() != BSON_STRING) {
    LOGE(TAG, "Missing 'color' string field in LED declaration => skip");
    return shared_ptr();
  }

  bson::Variant const& maxPWMVar = obj["max_pwm"];
  if (maxPWMVar.getType() != BSON_INT32) {
    LOGE(TAG, "Missing 'max_pwm' int32 field in LED declaration => skip");
    return shared_ptr();
  }

  return shared_ptr(new LED(colorVar.asString(), maxPWMVar.asInt32()));
}

PERIPHERAL_IMPLEMENT_GETTER(LED, getColor, std::string const&) { return _color; }

PERIPHERAL_IMPLEMENT_GETTER(LED, getMaxPWM, int32_t) { return _maxPWM; }

PERIPHERAL_IMPLEMENT_SETTER(LED, setPWM, int32_t pwm) {
  LOGD(TAG, "Setting PWM to %d", (int) pwm);
  bson::Object obj;
  writeHeader("set_pwm", obj);
  obj["value"] = pwm;
  send(obj);
}

} // namespace peripheral
} // namespace proxyos
