#include <proxyos/AbstractChannel.h>

#include <cstring>
#include <iostream>

#include <bson.hpp>

#include <proxyos/logger.h>

static char const* const TAG = "AbstractChannel";

namespace proxyos {

AbstractChannel::~AbstractChannel(void) {}

ssize_t
AbstractChannel::readBlocking(byte_t* buffer, std::size_t buffer_len) {
  std::size_t remaining = buffer_len;
  while (remaining > 0) {
    ssize_t ret = read(buffer, remaining, 1000);
    if (ret < 0) return ret;

    std::size_t uret = ret;
    if (uret > remaining) {
      LOGE(
          TAG,
          "Received too much data: %lu expected but received %lu",
          (long unsigned) remaining,
          (long unsigned) uret);
      return -1;
    }

    remaining -= uret;
    buffer += uret;
  }

  return buffer_len;
}

} // namespace proxyos
