#include <proxyos/Gateway.h>

#include <unistd.h>

#include <functional>

//#define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#include <proxyos/logger.h>

static char const* const TAG = "Gateway";

namespace proxyos {

Gateway::Gateway(AbstractChannel::shared_ptr const& channel)
    : _channel(channel) {
  _thread = std::thread(std::bind(&Gateway::worker, this));
}

Gateway::~Gateway(void) {
  if (!_disconnected) {
    disconnect();
    waitUntilDisconnected();
  }
}

typedef std::
    map<std::string, std::function<AbstractPeripheral::shared_ptr(bson::Object const& obj)>>
        builders_t;

static builders_t* builders = nullptr;

void
Gateway::addPeripheriralHandler(
    std::string const& type,
    std::function<AbstractPeripheral::shared_ptr(bson::Object const& obj)> const& builder) {
  LOGI(TAG, "Adding builder for type '%s'", type.c_str());
  if (!builders) { builders = new builders_t(); }
  (*builders)[type] = builder;
}

void
Gateway::send(bson::Object const& obj) {
  auto binary = bson::encode(obj);
  _channel->write(binary.data(), binary.size());
}

void
Gateway::disconnect(void) {
  LOGI(TAG, "Disconnecting gateway %s", _channel->getName().c_str());
  // TODO Send disconnection message
  _disconnected = true;
}

void
Gateway::waitUntilDisconnected(void) {
  // TODO Wait for disconnection ack
  while (!_disconnected) { sleep(1); }
  _thread.join();
}

void
Gateway::waitUntilPeripheralDeclaration(void) {
  // TODO Use conditions
  while (!_declared) { sleep(1); }
}

void
Gateway::worker(void) {
  discover();

  while (!_disconnected) {
    uint32_t bson_size = 0;
    _channel->read((proxyos::byte_t*) &bson_size, sizeof(bson_size), 1000);
    if (bson_size > 0) {
      std::unique_ptr<proxyos::byte_t[]> buffer(new proxyos::byte_t[bson_size]);
      memcpy(buffer.get(), &bson_size, sizeof(bson_size));
      ssize_t ret =
          _channel->readBlocking(buffer.get() + sizeof(bson_size), bson_size - sizeof(bson_size));

      if (ret > 0) {
        LOGD(TAG, "Received object:");
        LOGBSOND(TAG, buffer.get());

#if 0
        bson_print(buffer.get(), 0, 2);
        printf("\n");
        for (int i = 0; i < bson_size;) {
          for (int j = 0; j < 10 && i < bson_size; ++i, ++j) { printf("%02hhx ", buffer.get()[i]); }
          printf("\n");
        }
#endif

        bson::Object obj = bson::decode(buffer.get());

        bson::Variant const& tpVar = obj["tp"];
        if (tpVar.getType() != BSON_STRING) {
          LOGE(TAG, "Object should contain a field 'tp' of type BSON_STRING => skip");
          LOGBSONE(TAG, buffer.get());
          continue;
        }

        char const* tp = obj["tp"].asString();
        if (strcmp(tp, "peripheral_list") == 0) {
          onPeripheralList(obj);
        } else {
          bson::Variant const& prVar = obj["pr"];
          if (prVar.getType() != BSON_INT32) {
            LOGE(TAG, "A sensor data should contain 'pr' field with type BSON_INT32 => skip");
            LOGBSONE(TAG, buffer.get());
            continue;
          }

          int32_t pr = prVar.asInt32();
          auto it    = _peripherals.find(pr);
          if (it == _peripherals.end()) {
            LOGE(TAG, "Unable to find peripheral with id %d on current gateway => skip", (int) pr);
            LOGBSONE(TAG, buffer.get());
            continue;
          }

          it->second->onDataReceived(obj);
        }
      }
    }
  }
}

void
Gateway::discover(void) {
  LOGI(TAG, "Sending discover request");

  bson::Object obj;
  writeHeader("discover", obj);
  auto message = bson::encode(obj);
  _channel->write(message.data(), message.size());
}

void
Gateway::onPeripheralList(bson::Object const& obj) {
  bson::Variant const& peripheralsVar = obj["peripherals"];

  if (peripheralsVar.getType() != BSON_ARRAY) {
    LOGE(TAG, "Missing 'peripherals' array field");
    return;
  }

  bson::Object const& peripherals = peripheralsVar.asArray();
  for (auto const& pair : peripherals) {
    LOGD(TAG, "Peripheral '%s'", pair.first.c_str());

    if (pair.second.getType() != BSON_OBJECT) {
      LOGE(TAG, "Periphera declaration should be an object");
      continue;
    }

    bson::Object const& periphObj = pair.second.asObject();

    bson::Variant const& typeVar = periphObj["tp"];
    if (typeVar.getType() != BSON_STRING) {
      LOGE(TAG, "Missing 'tp' string field in peripheral declaration => skip");
      continue;
    }

    bson::Variant const& nameVar = periphObj["nm"];
    if (nameVar.getType() != BSON_STRING) {
      LOGE(TAG, "Missing 'nm' string field in peripheral declaration => skip");
      continue;
    }

    bson::Variant const& idVar = periphObj["id"];
    if (idVar.getType() != BSON_INT32) {
      LOGE(TAG, "Missing 'id' int32 field in peripheral declaration => skip");
      continue;
    }

    char const* type = typeVar.asString();
    AbstractPeripheral::shared_ptr peripheral;

    auto it = builders->find(type);
    if (it != builders->end()) {
      peripheral = (it->second)(periphObj);
    } else {
      LOGE(TAG, "Unable to build peripheral of type '%s'", type);
    }

    if (!peripheral) continue;

    peripheral->_parent = this;
    peripheral->_id     = idVar.asInt32();
    peripheral->_name   = nameVar.asString();

    LOGI(TAG, "New peripheral: %s", peripheral->toString().c_str());

    if (_peripherals.count(peripheral->_id)) {
      LOGE(TAG, "Peripheral with id %d already declared on this gateway => drop");
    } else {
      _peripherals[peripheral->_id] = peripheral;
    }
  }

  _declared = true;
}

void
Gateway::writeHeader(char const* type, bson::Object& obj) const {
  obj["tp"] = type;
}

void
Gateway::start(void) {
  LOGI(TAG, "Sending start request");

  bson::Object obj;
  writeHeader("start_all", obj);
  auto message = bson::encode(obj);
  _channel->write(message.data(), message.size());
}

void
Gateway::stop(void) {
  LOGI(TAG, "Sending stop request");

  bson::Object obj;
  writeHeader("stop_all", obj);
  auto message = bson::encode(obj);
  _channel->write(message.data(), message.size());
}

void
Gateway::sendDeviceTree(bson::Object const& deviceTree) {
  LOGI(TAG, "Sending set_device_tree request");

  bson::Object obj;
  writeHeader("set_device_tree", obj);
  (obj["device_tree"] = bson::Variant(BSON_OBJECT)).asObject() = bson::Object(deviceTree);

  auto message = bson::encode(obj);
  _channel->write(message.data(), message.size());
}

void
Gateway::restart(void) {
  LOGI(TAG, "Sending restart request");

  bson::Object obj;
  writeHeader("restart", obj);
  auto message = bson::encode(obj);
  _channel->write(message.data(), message.size());
}

} // namespace proxyos