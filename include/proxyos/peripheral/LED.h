#pragma once

#include <cstddef>
#include <string>

#include <proxyos/AbstractPeripheral.h>

namespace proxyos {
namespace peripheral {

class LED : public Peripheral<LED> {
  PERIPHERAL_DECLARE_TYPE(led);

  PERIPHERAL_DECLARE_GETTER(getColor, std::string const&);
  PERIPHERAL_DECLARE_GETTER(getMaxPWM, int32_t);
  PERIPHERAL_DECLARE_SETTER(setPWM, int32_t);

 public:
  LED(std::string const& color, int32_t maxPWM);

  std::string
  toString(void) const override;

 private:
  std::string _color;
  int32_t _maxPWM;
};

} // namespace peripheral
} // namespace proxyos
