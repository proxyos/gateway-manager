#pragma once

#include <cstddef>

#include <proxyos/AbstractPeripheral.h>

namespace proxyos {
namespace peripheral {

class Servomotor : public Peripheral<Servomotor> {
  PERIPHERAL_DECLARE_TYPE(servo);

  PERIPHERAL_DECLARE_SETTER(setDegree, double);
  PERIPHERAL_DECLARE_GETTER(getMaxDegree, double);

 public:
  Servomotor(double maxDegree);

  std::string
  toString(void) const override;

 private:
  double _maxDegree;
};

} // namespace peripheral
} // namespace proxyos
