#pragma once

#include <cstddef>
#include <string>

#include <proxyos/AbstractPeripheral.h>

namespace proxyos {
namespace peripheral {

class NTC : public Peripheral<NTC> {
  PERIPHERAL_DECLARE_TYPE(ntc);

  PERIPHERAL_DECLARE_GETTER(getUnit, std::string const&);
  PERIPHERAL_DECLARE_CALLBACK(atTemperature, int32_t temperature);

 public:
  NTC(std::string const& unit);

  std::string
  toString(void) const override;

 private:
  std::string _unit;
};

} // namespace peripheral
} // namespace proxyos
