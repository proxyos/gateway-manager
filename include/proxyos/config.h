#pragma once

#include <string>

namespace proxyos {

std::string
version(void);

} // namespace proxyos