#pragma once

#include <sys/types.h>

#include <cstddef>
#include <cstdint>
#include <memory>
#include <string>

namespace proxyos {

typedef char byte_t;

class AbstractChannel {
 public:
  typedef std::shared_ptr<AbstractChannel> shared_ptr;

  inline AbstractChannel(std::string const& name)
      : _name(name) {}

  virtual ~AbstractChannel(void);

  inline std::string const&
  getName(void) const {
    return _name;
  }

  virtual bool
  isValid(void) const = 0;

  virtual ssize_t
  readBlocking(byte_t* buffer, std::size_t buffer_len);

  virtual ssize_t
  read(byte_t* buffer, std::size_t buffer_len, int timeout_ms) = 0;

  virtual ssize_t
  write(byte_t const* buffer, std::size_t buffer_len) = 0;

  virtual void
  purge(void) = 0;

 private:
  std::string _name;
};

} // namespace proxyos
