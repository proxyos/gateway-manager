#pragma once

#include <cassert>
#include <functional>
#include <map>
#include <memory>
#include <string>

#include <bson.hpp>

namespace proxyos {

class Gateway;

class AbstractPeripheral {
 public:
  typedef std::shared_ptr<AbstractPeripheral> shared_ptr;
  typedef std::function<void(AbstractPeripheral*, bson::Object const& data)> data_callback_t;

  inline AbstractPeripheral(void)
      : _parent(NULL)
      , _id(-1) {}

  virtual ~AbstractPeripheral(void);

  virtual char const*
  getType(void) const = 0;

  virtual std::string
  toString(void) const = 0;

  inline std::string const&
  getName(void) const {
    return _name;
  }

  template<typename _Periph>
  bool
  is(void) const {
    return strcmp(getType(), _Periph::type) == 0;
  }

  template<typename _Periph>
  _Periph const&
  as(void) const {
    assert(is<_Periph>());
    return *static_cast<_Periph const*>(this);
  }

  template<typename _Periph>
  _Periph&
  as(void) {
    assert(is<_Periph>());
    return *static_cast<_Periph*>(this);
  }

  void
  at(std::string const& messageType, data_callback_t const& cb);

 protected:
  inline int32_t
  getId(void) const {
    return _id;
  }

  inline Gateway const*
  getParent(void) const {
    return _parent;
  }

  void
  writeHeader(char const* type, bson::Object& obj) const;

  void
  send(bson::Object const& obj);

  virtual void
  onDataReceived(bson::Object const& obj);

 private:
  friend class Gateway;

  Gateway* _parent;
  int32_t _id;
  std::string _name;
  std::map<std::string, std::vector<data_callback_t>> _callbacks;
};

template<typename _Class>
class Peripheral : public AbstractPeripheral {
 public:
  typedef _Class self;
};

} // namespace proxyos

#define PERIPHERAL_DECLARE_TYPE(_type)                             \
 public:                                                           \
  static constexpr char const* type = #_type;                      \
  inline char const* getType(void) const override { return type; } \
  static shared_ptr fromBson(bson::Object const& obj);

#define PERIPHERAL_IMPLEMENT_TYPE(_type, _obj)                              \
  static bool __init_##_type##__() {                                        \
    proxyos::Gateway::addPeripheriralHandler(_type::type, _type::fromBson); \
    return true;                                                            \
  }                                                                         \
                                                                            \
  static bool __start = __init_##_type##__();                               \
  _type::shared_ptr _type::fromBson(bson::Object const& _obj)

#define PERIPHERAL_DECLARE_GETTER(_fn, _ret) \
 public:                                     \
  _ret _fn(void) const

#define PERIPHERAL_IMPLEMENT_GETTER(_class, _fn, _ret) _ret _class::_fn(void) const

#define PERIPHERAL_DECLARE_SETTER(_fn, _val) \
 public:                                     \
  void _fn(_val)

#define PERIPHERAL_IMPLEMENT_SETTER(_class, _fn, _val) void _class::_fn(_val)

#define PERIPHERAL_DECLARE_CALLBACK(_fn, ...) \
 public:                                      \
  void _fn(std::function<void(self*, __VA_ARGS__)> const&)

#define PERIPHERAL_IMPLEMENT_CALLBACK(_class, _fn, _cb, ...) \
  void _class::_fn(std::function<void(_class::self*, __VA_ARGS__)> const& _cb)
