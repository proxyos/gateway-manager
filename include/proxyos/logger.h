#pragma once

#include <cstdarg>

#ifndef LOGGER_LOG_LEVEL
//#  define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_DEBUG
#  define LOGGER_LOG_LEVEL LOGGER_LOG_LEVEL_INFO
#endif

#define LOGGER_LOG_LEVEL_NONE (-1)
#define LOGGER_LOG_LEVEL_ERROR 0
#define LOGGER_LOG_LEVEL_WARNING 1
#define LOGGER_LOG_LEVEL_INFO 2
#define LOGGER_LOG_LEVEL_DEBUG 3

#define LOG(_level, _tag, _fmt, ...) \
  if (LOGGER_LOG_LEVEL >= _level) __logger(_level, _tag, __func__, _fmt, ##__VA_ARGS__)

#define LOGE(...) LOG(LOGGER_LOG_LEVEL_ERROR, ##__VA_ARGS__)
#define LOGW(...) LOG(LOGGER_LOG_LEVEL_WARNING, ##__VA_ARGS__)
#define LOGI(...) LOG(LOGGER_LOG_LEVEL_INFO, ##__VA_ARGS__)
#define LOGD(...) LOG(LOGGER_LOG_LEVEL_DEBUG, ##__VA_ARGS__)

#define LOGBSON(_level, _tag, _bson) \
  if (LOGGER_LOG_LEVEL >= _level) __logger_bson(_level, _tag, __func__, _bson)

#define LOGBSONE(...) LOGBSON(LOGGER_LOG_LEVEL_ERROR, ##__VA_ARGS__)
#define LOGBSONW(...) LOGBSON(LOGGER_LOG_LEVEL_WARNING, ##__VA_ARGS__)
#define LOGBSONI(...) LOGBSON(LOGGER_LOG_LEVEL_INFO, ##__VA_ARGS__)
#define LOGBSOND(...) LOGBSON(LOGGER_LOG_LEVEL_DEBUG, ##__VA_ARGS__)

namespace bson {
class Object;
}

namespace proxyos {

void
__logger_va(unsigned level, char const* tag, char const* func, char const* fmt, va_list args);

void
__logger(unsigned level, char const* tag, char const* func, char const* fmt, ...);

void
__logger_bson(unsigned level, char const* tag, char const* func, char const* data);

void
__logger_bson(unsigned level, char const* tag, char const* func, ::bson::Object const& obj);

} // namespace proxyos
