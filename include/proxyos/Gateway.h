#pragma once

#include <memory>
#include <thread>

#include <bson.hpp>

#include <proxyos/AbstractChannel.h>
#include <proxyos/AbstractPeripheral.h>

namespace proxyos {

class Gateway {
 public:
  typedef std::shared_ptr<Gateway> shared_ptr;

  Gateway(AbstractChannel::shared_ptr const& channel);

  ~Gateway(void);

  static void
  addPeripheriralHandler(
      std::string const& type,
      std::function<AbstractPeripheral::shared_ptr(bson::Object const& obj)> const& builder);

  void
  send(bson::Object const& obj);

  inline std::map<int32_t, AbstractPeripheral::shared_ptr> const&
  peripherals(void) const {
    return _peripherals;
  }

  void
  disconnect(void);

  void
  waitUntilDisconnected(void);

  void
  waitUntilPeripheralDeclaration(void);

  void
  start(void);

  void
  stop(void);

  void
  sendDeviceTree(bson::Object const& deviceTree);

  void
  restart(void);

  inline std::string
  getChannelName(void) const {
    return _channel ? _channel->getName() : "null";
  }

 private:
  void
  worker(void);

  void
  discover(void);

  void
  onPeripheralList(bson::Object const& obj);

  void
  writeHeader(char const* type, bson::Object& obj) const;

  AbstractChannel::shared_ptr _channel;
  std::thread _thread;
  std::map<int32_t, AbstractPeripheral::shared_ptr> _peripherals;

  bool _disconnected = false;
  bool _declared     = false;
};

} // namespace proxyos