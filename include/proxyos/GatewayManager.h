#pragma once

#include <map>
#include <string>
#include <vector>

#include <proxyos/AbstractPeripheral.h>
#include <proxyos/Gateway.h>

namespace proxyos {

class GatewayManager {
 public:
  Gateway::shared_ptr
  addGateway(std::string const& device);

  std::vector<AbstractPeripheral::shared_ptr>
  getPeripherals(void) const;

  void
  disconnectAll(void);

  void
  waitUntilAllDisconnected(void);

  void
  waitUntilAllPeriphalDeclaration(void);

  void
  startAll(void);

  void
  stopAll(void);

 private:
  std::map<std::string, Gateway::shared_ptr> _gateways;
};

} // namespace proxyos